create or replace FUNCTION FUN_CLIENT_PRO RETURN NUMBER AS 
v_rand number(10);
BEGIN
 select * into v_rand
 from (
	select client_id from  CLIENT_PRO
		order by dbms_random.value)
where rownum = 1;
return v_rand;
END FUN_CLIENT_PRO;
/
create or replace FUNCTION FUN_product_pro RETURN NUMBER AS 
v_rand number(10);
BEGIN
 select * into v_rand
 from (
	select product_id from  PRODUCTS_PRO
		order by dbms_random.value)
where rownum = 1;
return v_rand;
END FUN_product_pro;
/
  CREATE OR REPLACE FUNCTION FUN_product_pro_price (v_product_id PRODUCTS_PRO.product_id%type ) RETURN PRODUCTS_PRO.price_pice%type 
  AS 
v_price_pice PRODUCTS_PRO.price_pice%type;
BEGIN
  select price_pice into v_price_pice from PRODUCTS_PRO 
where product_id = v_product_id;
RETURN v_price_pice;
END FUN_product_pro_price;
/

  CREATE OR REPLACE FUNCTION FUN_product_pro_amount RETURN NUMBER AS 
v_amount NUMBER(3);
BEGIN
    select round (dbms_random.value(1, 20)) INTO v_amount  from dual;
  RETURN v_amount;
END FUN_product_pro_amount;
/

  CREATE OR REPLACE PROCEDURE PR_generate_one_position (v_order_id in ORDER_GENERAL_PRO.order_id%type,
					v_position in ORDER_POSITION_PRO.position_order%type)
 AS 
v_product_id number;
v_price_pice PRODUCTS_PRO.price_pice%type;
v_amount number;
BEGIN
    DBMS_OUTPUT.ENABLE;
    v_product_id := FUN_product_pro;
    v_price_pice := FUN_product_pro_price(v_product_id);
    v_amount := FUN_product_pro_amount;
    
    insert into ORDER_POSITION_PRO(order_id, position_order, price_sold, product_amount, product_id) VALUES (
    v_order_id,
    v_position,
    v_price_pice,
    v_amount,
    v_product_id
    );
END PR_generate_one_position;
/

  CREATE OR REPLACE PROCEDURE PR_generate_positions (v_order_id in ORDER_GENERAL_PRO.order_id%type) AS 
v_positions_amount number;
BEGIN
    select round( dbms_random.value ( 2, 10 )) INTO v_positions_amount from dual;
    for v_counter in 1..v_positions_amount
    loop
        PR_generate_one_position(v_order_id, v_counter);    
    end loop;

END PR_generate_positions;
/
create or replace PROCEDURE PR_generate_order (v_order_id out ORDER_GENERAL_PRO.order_id%type) AS 
BEGIN
  insert into ORDER_GENERAL_PRO(client_id,price_all) values(
  FUN_CLIENT_PRO,
  0
  );
   v_order_id := seq_order_pro.CURRVAL;
END PR_generate_order;
/
  CREATE OR REPLACE PROCEDURE PR_generate_random_order
  as
	v_order_id ORDER_GENERAL_PRO.order_id%type;
BEGIN
    PR_generate_order(v_order_id);
    PR_generate_positions(v_order_id);
END PR_generate_random_order;
/


create view per_product_income as
SELECT product_name as "Name", category_name as category_name ,SUM(product_amount) as product_amount, ORDER_POSITION_PRO.price_sold*SUM(product_amount) as Income 
from CATEGORIES_PRO,ORDER_POSITION_PRO, PRODUCTS_PRO WHERE 
ORDER_POSITION_PRO.product_id = PRODUCTS_PRO.product_id
and CATEGORIES_PRO.category_id = PRODUCTS_PRO.category_id
group by product_name,ORDER_POSITION_PRO.price_sold,category_name
order by  Income desc;

create view orders_last_month as
SELECT  *
 FROM ORDER_GENERAL_PRO
 where order_date between sysdate-30 and sysdate
order by order_date;

create view clients_ranking as
SELECT client_name as "Name" ,client_surname as "Surname", count(*) as "Orders amount" , ROUND(SUM(price_all),2) as Income_from_client from 
ORDER_GENERAL_PRO, CLIENT_PRO
WHERE ORDER_GENERAL_PRO.client_id = CLIENT_PRO.client_id
GROUP BY client_name,client_surname
order by Income_from_client desc;

create view income_in_current_month as
SELECT  DISTINCT Extract(year from order_date) as  year_now, to_char(sysdate, 'Month') as month_now, 
count(order_date) as orders_amount, 
(select ROUND(sum(price_all),2) from ORDER_GENERAL_PRO where Extract(month from order_date)=Extract(month from sysdate)) as Income
FROM ORDER_GENERAL_PRO
where Extract(month from order_date)=Extract(month from sysdate)
group by Extract(year from order_date)
order by year_now,Income desc;
